package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"strings"

	"github.com/nidev/exifmarker/appaux"
	"github.com/nidev/exifmarker/marker"
)

// OutputPrefix will be used as a prefix for output filename
const OutputPrefix = "TAGGED_"

// AppVerMajor represents major version of sementic versioning
const AppVerMajor = 0

// AppVerMinor represents minor version of sementic versioning
const AppVerMinor = 0

// AppVerPatch represents patch level of sementic versioning
const AppVerPatch = 1 //& appaux.VersionData{0, 0, 1}

func appGreeting(v *appaux.VersionData) {
	fmt.Println("ExifMarker", *v)
	fmt.Println("Imprint Exif information on image")
}

func doFileOperation(src, dst string, dryRun bool) {
	log.Println("Processing", src)
	log.Println("(Output destination: ", dst, ")")
	log.Println("Dry-run mode:", dryRun)

	// Extract Exif Here (Call commons)
	exifDic, exifErr := marker.Decode(src)

	if exifErr != nil {
		log.Println("Exif parsing error")
		log.Println(exifErr)
		return
	}

	// Write Image label Here (Call commons)
	if !dryRun {
		if err := marker.LabelOn(src, dst, &exifDic); err != nil {
			log.Println("Error during processing image:")
			log.Println(err)
		}
	}

	log.Println("Finished")
}

func doWebService(host string, port uint, rootDir, tempDir string) {
	err := serveInfinitely(host, port, rootDir, tempDir)

	if err != nil {
		// TODO: API for printing error message
		// TODO: Logging interface?
		log.Println("Server didn't halt gracefully")
		log.Println(err)
	} else {
		log.Println("Server halted gracefully")
	}
}

// ExcludingTags variable manages exemptions of Exif information
var ExcludingTags []string

func main() {
	var srcPath string
	var dstPath string
	var tagExList string
	var dryRunFlag bool
	var wsMode bool
	var wsHost string
	var wsPort uint
	var ver = appaux.VersionData{Major: AppVerMajor, Minor: AppVerMinor, PatchLevel: AppVerPatch}

	flag.BoolVar(&dryRunFlag, "dry", false, "Dry-run flag, won't perform real task if on")
	flag.StringVar(&srcPath, "src", "", "Source file path/name, required")
	flag.StringVar(&dstPath, "dst", "", "Output file path/name, optional")
	flag.StringVar(&tagExList, "except", "", "Exif tag to be excluded, comma-separated, optional")
	flag.BoolVar(&wsMode, "wsmode", false, "Run as a web service. All options except web service options will be ignored")
	flag.UintVar(&wsPort, "wsport", 8080, "Port, optional for web service mode")
	flag.StringVar(&wsHost, "wshost", "0.0.0.0", "Host to be bound, optional for web service mode")

	flag.Parse()

	appGreeting(&ver)

	if !flag.Parsed() || (!wsMode && srcPath == "") {
		flag.PrintDefaults()
		os.Exit(-1)
	}

	ExcludingTags = strings.Split(tagExList, ",")

	for i, s := range ExcludingTags {
		ExcludingTags[i] = strings.TrimSpace(s)
	}

	if wsMode {
		// STUB: Dir paths
		doWebService(wsHost, wsPort, ".", ".")
	} else {
		// Sanitize paths
		srcPath = path.Clean(srcPath)

		if dstPath == "" {
			// TODO: Split basepath and filename, and prepend OutputPrefix
			dstPath = OutputPrefix + path.Base(srcPath)
		}

		doFileOperation(srcPath, dstPath, dryRunFlag)
	}
}
