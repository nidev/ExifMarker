package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gobuffalo/packr"
	"github.com/nidev/exifmarker/appaux"
	"github.com/nidev/exifmarker/marker"
)

var serverStat = appaux.ServerStat{StartedAt: time.Now()}
var box = packr.NewBox("./fe-src/dist/")
var jobIDManager = appaux.NewIDManager(2000)

func renderMain(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte(box.String("index.html")))
}

func renderResult(res http.ResponseWriter, req *http.Request) {
	panic("not implemented")
}

func renderError(res http.ResponseWriter, req *http.Request, msg string) {
	res.Write([]byte("ERROR: " + msg))
}

func createNewJob(res http.ResponseWriter, req *http.Request) {
	// TODO: assign job ID, create job ID manager
	var jobID = jobIDManager.NextStr()
	var msgPrefix = fmt.Sprintf("[%v@%v]", time.Now(), req.Host)

	log.Println(msgPrefix, "Assign new Job: ", jobID)
	msgPrefix += ":" + jobID

	jlog := func(item ...interface{}) {
		log.Print(msgPrefix)
		log.Println(item...)
	}

	if req.Method == "POST" && req.Header["Content-Type"] != nil {
		// TODO: Read form data, multipart data
		// TODO: Choose good memory size for Request.ParseMultipartForm()
		if req.Header["Content-Type"][0] == "multipart/form-data" && req.ParseMultipartForm(1<<32) == nil {
			file, header, formErr := req.FormFile("srcImage")

			if formErr != nil {
				res.Write([]byte(box.String("error.gohtml")))
			}

			jlog("File Header information")
			jlog("SIZE =", header.Size)
			jlog("MIME =", header.Header)
			jlog("NAME =", header.Filename)

			if header.Size < 0 {
				jlog("Suspicious file size detected. Job halted")
				renderError(res, req, "Undefined file size")
				return
			}

			// TODO: check maximum image size
			// Save to local first and work
			var srcData, _ = ioutil.ReadAll(file)
			var tmpFileErr = ioutil.WriteFile("foxtrot", srcData, 0)

			if tmpFileErr != nil {
				jlog("Can not create new file in temporary directory")
				jlog(tmpFileErr)
				renderError(res, req, "Server error (Storage)")
				return
			}

			// Extract Exif Here (Call commons)
			exifDic, exifErr := marker.Decode("foxtrot")

			if exifErr != nil {
				jlog("Critical error occured while processing Exif data")
				jlog(exifErr)
				renderError(res, req, "Server error (Exif Parser Error)")
			}

			// Write Image label Here (Call commons)
			// TODO: Distinguish basepath and filename, and prepend OutputPrefix
			marker.LabelOn("foxtrox", OutputPrefix+"foxtrot", &exifDic)
			// Move to webRoot Here
			renameErr := os.Rename("foxtrot", "new/foxtrot")

			if renameErr != nil {
				jlog("Can not move processed to file to web root directory")
				jlog(renameErr)
				renderError(res, req, "Server error (after processing)")
				return
			}

			// TODO: additional variables like output image path
			renderResult(res, req)
		} else {
			renderError(res, req, "Incorrect content type")
		}
	} else {
		renderError(res, req, "Unsupported HTTP method")
	}
}

func renderCredit(res http.ResponseWriter, req *http.Request) {
	panic("not implemented")
}

func renderLicenses(res http.ResponseWriter, req *http.Request) {
	panic("not implemented")
}

func serveWebpackScript(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte(box.String("scripts/fe-src.js")))
}

func serveInfinitely(host string, port uint, rootDir, tempDir string) error {
	var router = http.NewServeMux()

	s := &http.Server{
		Addr:           fmt.Sprintf("%v:%v", host, port),
		Handler:        router,
		ReadTimeout:    10 * time.Second, // TODO: Make it adjustable
		WriteTimeout:   10 * time.Second, // TODO: Make it adjustable
		MaxHeaderBytes: 1 << 20,
	}

	// Configure URL router
	router.HandleFunc("/", renderMain)
	router.HandleFunc("/index.html", renderMain)
	router.HandleFunc("/scripts/fe-src.js", serveWebpackScript)
	router.HandleFunc("/credit", renderCredit)
	router.HandleFunc("/licenses", renderLicenses)
	router.HandleFunc("/new", createNewJob)
	router.HandleFunc("/result/", renderResult)

	err := s.ListenAndServe()

	if err != nil {
		log.Fatal(err)
	}

	return err
}
