package appaux

// IDManager is used for managing ID namespace for some kind of job management
type IDManager struct {
	currentID, seed int64
}

// NewIDManager creates new namespace pool (incremental)
func NewIDManager(seed int64) *IDManager {
	return &IDManager{currentID: 0, seed: seed}
}

// NextInt returns new ID in 64-bit signed integer
func (i *IDManager) NextInt() int64 {
	// STUB
	return 0
}

// NextStr returns new ID from 64-bit signed integer, exported to string
func (i *IDManager) NextStr() string {
	// STUB
	return "STUB"
}
