package appaux

import "fmt"

// VersionData is a struct type for representing current application version
type VersionData struct {
	Major, Minor, PatchLevel uint16
}

func (v VersionData) String() string {
	return fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.PatchLevel)
}
