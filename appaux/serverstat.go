package appaux

import "time"

// ServerStat stores req/res counts
type ServerStat struct {
	Requests    uint64
	Responses   uint64
	JobsCurrent uint32
	JobsDone    uint32
	JobsFail    uint32
	StartedAt   time.Time
}
