import React from 'react';
import { connect } from 'react-redux';

import Enqueuer from '../../components/Enqueuer';
import { updateCountRequest } from '../../actions';

class Home extends React.Component {
  render() {
    // const {
    //   status,
    //   count,
    //   updateCount,
    // } = this.props;

    return (
      <div>
        <p>여기 위에 메뉴를 렌더링</p>
        <p>파일업로드/파일 삭제/오픈소스 소프트웨어 정보/방명록</p>
        <Enqueuer />
        <p>여기에 소프트웨어 이름, 버전 정보 하단 막대 표시</p>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
