import React from 'react';
import { connect } from 'react-redux';

class Enqueuer extends React.Component {
  render() {
    return (<div>파일 업로드하기</div>);
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    status: state.someReducer.status,
    count: state.someReducer.count,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    updateCount: (newCount) => {
      dispatch(updateCountRequest(newCount));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Enqueuer);
