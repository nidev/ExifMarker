package marker

import (
	"log"
	"os"

	"github.com/rwcarlsen/goexif/exif"
	"github.com/rwcarlsen/goexif/mknote"
)

// Decode decodes something
// Returns map[string]string, value string "error" on failed item
func Decode(path string) (map[exif.FieldName]string, error) {
	f, err := os.Open(path)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	// Optionally register camera makenote data parsing - currently Nikon and
	// Canon are supported.
	exif.RegisterParsers(mknote.All...)

	x, err := exif.Decode(f)

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	dataDic := make(map[exif.FieldName]string)

	predefinedFieldList := [...]exif.FieldName{
		exif.Model,
		exif.DateTime,
		exif.LensModel,
		exif.FocalLength,
		exif.ISOSpeedRatings,
		exif.ExposureTime,
		exif.ApertureValue,
		exif.Flash}

	for _, v := range predefinedFieldList {
		exifData, exifErr := x.Get(v)

		if exifErr != nil {
			dataDic[v] = "error"
		} else {
			dataDic[v] = exifData.String()
		}
	}

	return dataDic, nil
}
