package marker

import (
	"bufio"
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"log"
	"math"
	"os"
	"strings"

	"github.com/rwcarlsen/goexif/exif"

	"golang.org/x/image/font/basicfont"

	"github.com/fogleman/gg"
	"github.com/gobuffalo/packr"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
)

// TTFPackrBox includes embedded Truetype font for ExifMarker (global)
var TTFPackrBox = packr.NewBox("../font")

func loadEmbeddedFont(points float64) (font.Face, error) {
	fontBytes := TTFPackrBox.Bytes("FiraMono-Medium.ttf")

	f, err := truetype.Parse(fontBytes)

	if err != nil {
		return nil, err
	}

	face := truetype.NewFace(f, &truetype.Options{
		Size: points,
		// Hinting: font.HintingFull,
	})
	return face, nil
}

func calcFontSize(w, h int) float64 {
	var defaultSize = 8.0
	var minSize = math.Min(float64(w), float64(h))
	var calcSize = 0.0

	if minSize > 100.0 && minSize < 1000.0 {
		calcSize = minSize * 0.04
	} else {
		calcSize = minSize * 0.02
	}

	return math.Max(defaultSize, calcSize)
}

func formatStringFractionToDecimalString(s string) string {
	s = strings.Replace(s, "\"", "", -1)
	s = strings.TrimSpace(s)

	var a, b int

	fmt.Sscanf(s, "%v/%v", &a, &b)

	return fmt.Sprintf("%.1f", math.Ceil(float64(a)*10/float64(b))/10.0)
}

func saveJPEG(dstPath string, im image.Image) error {
	// Original Code: https://github.com/fogleman/gg/blob/master/util.go
	// TODO: May write PR later
	file, err := os.Create(dstPath)

	if err != nil {
		return err
	}

	defer file.Close()

	dstBuf := bufio.NewWriter(file)

	defer dstBuf.Flush()

	return jpeg.Encode(dstBuf, im, &jpeg.Options{Quality: 100})
}

// LabelOn adds labels from Exif information dictionary
func LabelOn(srcPath, dstPath string, exifDic *map[exif.FieldName]string) error {
	var srcImg, srcImgErr = gg.LoadImage(srcPath)

	if srcImgErr != nil {
		log.Println("Error while opening source image from path:", srcPath)
		log.Println(srcImgErr)
		return srcImgErr
	}

	dc := gg.NewContextForImage(srcImg)

	var (
		baseHeight = calcFontSize(dc.Width(), dc.Height())
	)

	embFont, embFontErr := loadEmbeddedFont(baseHeight)

	if embFontErr == nil {
		dc.SetFontFace(embFont)
	} else {
		dc.SetFontFace(basicfont.Face7x13)
	}

	exifDigestText := fmt.Sprintf("%s %s / ISO %s / %ssec / F%s (approx.)",
		(*exifDic)[exif.Model],
		(*exifDic)[exif.LensModel],
		(*exifDic)[exif.ISOSpeedRatings],
		(*exifDic)[exif.ExposureTime],
		formatStringFractionToDecimalString((*exifDic)[exif.ApertureValue]))

	exifDigestText = strings.Replace(exifDigestText, "\"", "", -1)

	// calculate center position, in other words, recalculate starting point of
	// DrawString().

	// Assume that we don't have 2-byte character to write
	var assumedWidth = 0

	for _, r := range exifDigestText {
		awidth, ok := embFont.GlyphAdvance(r)

		if ok {
			assumedWidth += int(float64(awidth) / 64)
		}
	}

	if assumedWidth == 0 {
		assumedWidth = dc.Width()
	}

	centerPointXBegin := (dc.Width() - assumedWidth) / 2
	dc.SetColor(color.Black)
	dc.DrawString(exifDigestText, float64(centerPointXBegin), float64(dc.Height())-baseHeight-1)
	dc.SetColor(color.White)
	dc.DrawString(exifDigestText, float64(centerPointXBegin), float64(dc.Height())-baseHeight)

	return saveJPEG(dstPath, dc.Image())
}
